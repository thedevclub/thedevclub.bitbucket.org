var BLOG = function(){
    
    console.log('blog');
    
    this.setUpMasonry();
    this.setUpPosts();
    this.setUpWow();
}; 

BLOG.prototype.setUpMasonry = function(){
    
    $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        isResizable: true,
        isAnimated: true,
    });
};

BLOG.prototype.setUpPosts = function(){
    // Set up blog posts 
    this.posts = $('.grid-item');

    this.posts.addClass('zoomIn');
    
    this.posts.hover(function(){
        // in
        var $this = $(this);
        $this.addClass("hover");
        TweenMax.fromTo($this.find(".mask"),0.6,{opacity:0,display:'none'},{opacity:0.7,display:'block'});
        TweenMax.fromTo($this.find(".info"),0.6,{opacity:0,display:'none'},{opacity:1,display:'table-cell'});
    },function(){
        // out
        var $this = $(this);
        $this.removeClass("hover");
        TweenMax.to($this.find(".mask"),0.6,{opacity:0,display:'none'});
        TweenMax.to($this.find(".info"),0.6,{opacity:0,display:'none'});
    }); 
};

BLOG.prototype.setUpWow = function(){
    
    wow = new WOW({
                  boxClass:     'grid-item',      // default
                  animateClass: 'animated', // default
                  offset:       200,        
                  mobile:       true,       // default
                  live:         true        // default
                });
    wow.init();
};